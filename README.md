## Docker Desktop em Ambiente Windows 64 bits

O guia abaixo tem por objetivo ensinar a instalar o Docker em ambiente Windows. Com o docker devidamente instalado e configurado vamos posteriormente portar o back-end para este ambiente. Este guia é APENAS para ambiente Windows (64 Bits).

**NOTA:** _Caso deseje ter o docker no ambiente Linux, recomendo que reveja os guias que foram produzidos na **COM310 - Infarestrutura para Sistemas de Software - 3o Bimestre.** Lá eu detalho todo o processo de instalação no Linux Ubuntu 20.04. Para fins de funcionalidade, a aplicação que estamos desenvolvendo neste curso vai funcionar tanto com docker no ambiente Linux quanto no Windows._

## Pré-Requisitos

O primeiro passo é ter em mente que o seu hardware (desktop ou notebook) precisa ter suporte à virtualização. E que este suporte esteja devidamente habilitado no BIOS do seu dispositivo. Por isso, antes de prosseguir, **HABILITE** o suporte à virtualização no seu dispositivo, caso ele seja um hardware que tenha este suporte. Após isso, vamos prosseguir com o restante do guia.

Após a verificação sugerida anteriormente, o próximo passo é ter certeza de que o Hyper-V está habilitado em seu computador. Prossiga conforme indicado na figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre01.png" alt="Instalacao do docker."/>
</p>

Clique em **Programas** conforme indicado abaixo.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre02.png" alt="Instalacao do docker."/>
</p>

Prossiga, clicando em **Ativar/Desativar** Recursos do Windows.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre03.png" alt="Instalacao do docker."/>
</p>

Na imagem que segue, procure pelo _Hyper-V_ e **HABILITE-O**. Em seguida, clique em **OK**.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre04.png" alt="Instalacao do docker."/>
</p>

### Download do Docker

Após clicar em OK, será pedido para **reiniciar o computador**. Prossiga com a _Reinicialização_. Após a reinicialização, faça uma busca no Google com a seguinte string: **install docker for windows**. Clique no primeiro link, que aparece na busca: https://docs.docker.com/desktop/windows/install/. Prossiga com do download do docker conforme a figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre05.png" alt="Instalacao do docker."/>
</p>

### Instalação do Docker 

Após o download, você deve executar **como administrador** o **.exe** baixado, como mostrado abaixo. Quando perguntado na próxima tela, clique em **SIM**.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre06.png" alt="Instalacao do docker."/>
</p>

O processo de instalação deve prosseguir sem nenhum problema e o resultado será a tela apresentada na figura abaixo. O próximo passo é observar a imagem a seguir. Matenha os checkbox preenchidos, conforme já indicado na instalação e dê OK.

<p align="center">
  <img target="_blank" src="./imagens/docker-win2.png" alt="Instalacao do docker."/>
</p>

Após clicar em OK o processo de instalação inicia conforme a figura abaixo. Todo o processo pode demorar um pouco. Aguarde um instante até que ele finalize.

<p align="center">
  <img target="_blank" src="./imagens/docker-win3.png" alt="Instalacao do docker."/>
</p>

Após o processo finalizar, você vai ver uma tela como mostrada seguir. **Reinicie o computador** como é solicitado.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre07.png" alt="Instalacao do docker."/>
</p>

Após clicar em **Close** para fechar a tela de instalação, procure pelo ícone do Docker em sua Área de Trabalho e dê um **duplo click** neste ícone para iniciar o docker.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre08.png" alt="Instalacao do docker."/>
</p>

**Marque o checkbox** conforme indicado na seta e clique em **OK**, aceitando os termos de serviços do docker.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre09.png" alt="Instalacao do docker."/>
</p>

A próxima tela vai indicar que falta o WSL Linux Kernel. Você pode clicar em **Cancel**, pois neste caso ao invés de termos um ambiente com o kernel linux dentro do Windows, vamos utilizar o ambiente já ajustado e habilitado com o Hyper-V.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre10.png" alt="Instalacao do docker."/>
</p>

O próximo passo é acessarmos as configurações do ambiente docker **para desabilitarmos o WSL** e mantermos apenas a integração com o Hyper-v. Prossiga conforme indicado na tela abaixo.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre11.png" alt="Instalacao do docker."/>
</p>

Verifique que a engine WSL 2 está habilitada. Ela **não pode ficar habilitada** como a figura a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre12.png" alt="Instalacao do docker."/>
</p>

E **deve ser desabilitada** conforme a figura abaixo. Depois de desmarcar para desabilitar, clique em **Accept & Restart**.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre13.png" alt="Instalacao do docker."/>
</p>

Até que o processo de desabilitação finalize, você terá uma figura como mostrada abaixo:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre14.png" alt="Instalacao do docker."/>
</p>

Você pode clicar em Start para fazer um pequeno tutorial em que é mostrado como fazer download de uma imagem, interagir com a mesma, etc. Mas também pode clicar em Skip Tutorial.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre15.png" alt="Instalacao do docker."/>
</p>

Se clicar em Skip Tutorial, terá a tela descrita abaixo. Desta forma podemos verificar que temos o docker em execução conforme indicado no ícone em verde logo na parte inferior esquerda como destacado na seta.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre16.png" alt="Instalacao do docker."/>
</p>

Vamos também fazer um teste pelo PowerShell do Windows e verificar que o docker responde a alguns simples comandos. Abra o PowerShell conforme indicado a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre17.png" alt="Instalacao do docker."/>
</p>

Execute primeiro o comando para conhecer a versão instalada do docker.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre18.png" alt="Instalacao do docker."/>
</p>

Em seguida, digite docker e terá diversas opções no menu. Isso indica que temos todo o ambiente preparado para que possamos transferir o ambiente do back-end para uma imagem docker. Com isso na Semana 7 poderemos implantar esta imagem na nuvem da Google e também da Azure.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre19.png" alt="Instalacao do docker."/>
</p>

### Validação da Instalação do Docker

Depois de instalado, o Docker Desktop é executado automaticamente como um serviço. Ele é mostrado no System Tray quando você faz login no Windows após a reinicialização. Mas como realmente sabemos que está funcionando? Para verificar se o Docker Desktop está funcionando já abrimos anteriormente um console de linha de comando e executamos o comando docker. Como certificamos, a instalação ocorreu de forma adequada, pois vimos uma referência de comando do Docker.

Por fim, para certificarmos que de fato podemos criar imagens e lidar com elas, vamos executar uma imagem de contêiner de exemplo chamada hello-world, executando o comando _docker run hello-world_. Abra um console (pode ser o PowerShell ou o CMD) e execute o comando a seguir:

```
docker run hello-world
```

E teremos a saída abaixo:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre20.png" alt="Instalacao do docker."/>
</p>

### Preparação de Imagem Linux com o Laravel

O objetivo desta etapa do tutorial é ter o todo o código do back-end com o ambiente Laravel devidamente implantado em uma imagem Linux Ubuntu para que na Semana 7 possamos fazer o deploy desta imagem na Google e também na Azure. O objetivo é que tenhamos o back-end remoto em uma nuvem computacional e o front-end funcional localmente (no seu desktop ou notebook) como foi desenvolvido na Semana 3. Alguns ajustes no front-end serão feitos para que isso possa ser possível e vamos mostrar isso mais adiante.

Considerando que você seguiu todos os passos anterioremente e tem o docker funcionando, vamos baixar uma imagem Ubuntu do repositório do docker hub. Abrar um PowerShell e prossiga com descrito na tela a seguir. 

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre21.png" alt="Instalacao do docker."/>
</p>

Com o comando docker imagens podemos verificar a imagem que acabmos de baixar. Observe que o tamanho da imagem Linux é bem pequeno. Mas esse tamanho vai crescer bastante pois teremos que instalar e configurar vários pacotes.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre22.png" alt="Instalacao do docker."/>
</p>

Agora o que precisamos fazer é iniciar o container com a imagem baixada. Depois da execução do comando apresentado na tela abaixo você vai ganhar posse do terminal e nele fará tudo o que precisa para ter o back-end com laravel todo funcional. 

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre23.png" alt="Instalacao do docker."/>
</p>

É importante que você não saia do terminal até que tenhamos instalado dos os pacotes. Assim, **NÃO DIGITE** _exit_ no terminal, pois você perderá todas as alterações feitas na imagem, até que façamos um commit ao final. Prossiga com a atualizaçao com o comando apt-get update como indicado na sequência.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre24.png" alt="Instalacao do docker."/>
</p>

O próximo passo é instalar algumas ferramentas que nos darão suporte para saber o IP da imagem, etc.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre25.png" alt="Instalacao do docker."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre26.png" alt="Instalacao do docker."/>
</p>

A partir deste ponto o que farei é basicamente repetir os passos para instalar e configurar o Apache, PHP, MySQL e Composer como aprendemos no Guia da Semana 2. Então o que você deve fazer é seguir os passos do Guia da Semana 2 para Linux e considerar apenas os itens:

```
1)
2)
3)
4)
5)
7)
8)
```

Para faciliar, eu vou mostrar os passos tela a tela como destacado a seguir.  

Vamos instalar o Apache!

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre27.png" alt="Instalacao do docker."/>
</p>

Execute o comando abaixo para verificar que o Apache não está em execução.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre28.png" alt="Instalacao do docker."/>
</p>

Prossiga com o comando a seguir para colocar o Apache em execução.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre29.png" alt="Instalacao do docker."/>
</p>

Verifique novamente o status com o comando abaixo e verá que o Apache está rodando corretamente.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre30.png" alt="Instalacao do docker."/>
</p>

Vamos agora instalar o MySql Server!

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre31.png" alt="Instalacao do docker."/>
</p>

Prossiga com as modificações na senha padrão do root do MySQL. Escolha um padrão de tamanho de senha adequada para você e quando solicitado a senha, utilize (por exemplo) esta senha: **Gio2017!)** . _Esta é apenas uma dica de senha!!!_

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre32.png" alt="Instalacao do docker."/>
</p>

Inicie o mysql com o comando abaixo:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre33.png" alt="Instalacao do docker."/>
</p>

A próxima etapa é instalar o PHP. Vamos em frente com o comando a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre34.png" alt="Instalacao do docker."/>
</p>

Na sequência podemos verificar com o comando abaixo a versão recentemente instalada.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre35.png" alt="Instalacao do docker."/>
</p>

A seguir instale o curl!

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre37.png" alt="Instalacao do docker."/>
</p>

Agora chegou o momento de baixar o composer!!

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre36.png" alt="Instalacao do docker."/>
</p>

Prossiga com a instalação do composer!

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre38.png" alt="Instalacao do docker."/>
</p>

Por último, precisamos configurar o servidor web Apache para hospedar o site Laravel. Para que isso funcione, precisamos criar um arquivo host virtual. Execute o comando abaixo:

```
pico /etc/apache2/sites-available/laravel.conf
```

E adicione as linhas abaixo: 

```
<VirtualHost *:80>
ServerName localhost
ServerAdmin admin@example.com
DocumentRoot /var/www/html/laravel-crud-app/public
<Directory /var/www/html/laravel-crud-app>
AllowOverride All
</Directory>
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Salve e feche o arquivo. Para salvar o arquivo tecle: **CRTL+X** e depois **Y** e em seguida **ENTER**.

Em seguida, habilite o site do Laravel e o módulo de reescrita do Apache usando estes dois comandos.

```
a2ensite laravel.conf
```

```
a2enmod rewrite
```

Caso esteja seguindo os passos que indiquei lá do Guia da Semana 2, você não precisa executar o restante dos comandos depois do a2enmod. 

O próximo passo é instalarmos o Node.js. O Node.js é um ambiente de execução do JavaScript criado para a programação do lado do servidor e que permite aos desenvolvedores criarem funcionalidades de back-ends utilizando o JavaScript. Para baixar o script de instalação do Node.js no terminal do Linux da nossa imagem docker, execute o comando:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre44.png" alt="Instalacao do docker."/>
</p>

Execute o comando a seguir para que o Node.js esteja disponível no path global do Linux.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre46.png" alt="Instalacao do docker."/>
</p>

Prossiga para ver lista de versões disponíveis.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre47.png" alt="Instalacao do docker."/>
</p>

E instale a versão indicada abaixo:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre48.png" alt="Instalacao do docker."/>
</p>

Certifique-se depois da versão que acabamos de instalar.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre49.png" alt="Instalacao do docker."/>
</p>

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre50.png" alt="Instalacao do docker."/>
</p>

O próximo passo é instalar o npm, que é o gerenciador de pacotes do Node.js.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre51.png" alt="Instalacao do docker."/>
</p>

Como muitos pacotes foram instalados precisamos salvar todas essas alterações efetuadas na imagem do Ubuntu. **Matenha esse PowerShell aberto. **

**Abra outro PowerShell ou o CMD do Windows** e execute o seguinte comando:

```
docker ps
```

Você verá na saída deste comando que o container tem um ID. Copie este ID e prossiga com o comando a seguir no novo shell que acabou de abrir.

```
docker commit -m "Laravel Crud - Version-1.0" -a "seu nome" ID-DO-SEU-CONTAINER seunome/com320-app-v1.0
```

Este processo pode demorar um pouco até finalizar. Após finalizado, execute o comando:

```
docker images
```

E terá uma saída parecida com esta:

```
REPOSITORY                        TAG       IMAGE ID       CREATED        SIZE
julio/com320-app-v1.0             latest    930b815d6fb1   2 hours ago    2.05GB
ubuntu                            latest    1318b700e415   3 months ago   72.8MB
hello-world                       latest    d1165f221234   8 months ago   13.3kB
```

Pronto, feito isso, agora **você pode fechar o primeiro PowerShell** que utilizamos para instalar todos os pacotes.

### Execução da Imagem Atualizada, Abertura de Portas e Cópia do Projeto laravel-crud-app da Semana 5 

O que temos até o momento é uma imagem Linux toda customizada para pode executar um back-end com Laravel e ferramentas correlatas. O que precisamos fazer é seguir alguns passos e o primeiro deles é copiar os arquivos do host (no nosso caso o Windows) para a imagem Linux que atualizamos.

No novo PowerShell, você pode agora executar a nova imagem, já atualizada. Como estamos no Windows, eu considero que o projeto laravel-crud-app está na pasta: **C:\laragon\www**.

```
docker run --mount type=bind,source="C:/laragon/www/",target=/home/julio -it julio/com320-app-v1.0 
```

O resultado da execução do comando anterior pode ser visto a seguir

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre52.png" alt="Instalacao do docker."/>
</p>

Depois de feito procedimento de montagem remoto, você pode acessar a pasta no docker que está sendo mapeada do host (Windows). No meu caso eu pedi para montar em: /home/julio. O que temos que fazer é fazer a cópia do laravel-crud-app para a pasta do apache em: /var/www/html. Primeiro eu entro em /home/julio:

```
cd /home/julio
```

Depois executo o comando de cópia como a seguir:

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre53.png" alt="Instalacao do docker."/>
</p>

Na sequência vamos acessar a pasta /var/www/html e ter certeza que a cópia dos arquivos foi feita corretamente.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre54.png" alt="Instalacao do docker."/>
</p>

O resultado é mostrado a seguir.

<p align="center">
  <img target="_blank" src="./imagens/docker-win-pre55.png" alt="Instalacao do docker."/>
</p>

**Matenha o PowerShell aberto sem digitar exit!!!**

#### Atualização do Composer e validação da migração da aplicação laravel

Quando copiamos um projeto Laravel de um host para outro, ou seja da nossa máquina local para um servidor em produção, temos que ajustar algumas configurações para que a aplicação rode adequadamente. 

Ainda dentro da imagem Linux, acesse a pasta **/var/www/html/laravel-crud-app**.

```
cd /var/www/html/laravel-crud-app
```

Na sequência execute o comando:

```
compose update
```

Este processo pode demorar um pouco!!

Considerando que esteja dentro da pasta **laravel-crud-app**, execute também o comando a seguir para ajustar problemas de permissão de arquivos do laravel.

```
chmod -R 777 bootstrap/cache storage
```

Ainda na pasta **laravel-crud-app**, você deve acessar o arquivo **.env** para verificarmos se as configurações da base de dados que fizemos na Semana 5 estão OK e são as mesmas depois da cópia para a imagem docker.

```
pico .env
```

Verifique basicamente as linhas abaixo e constate que usuário, senha, porta estão OK.

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel_db
DB_USERNAME=root
DB_PASSWORD=Gio2017!)
```

Agora vamos prosseguir na linha de comando para limpar o ambiente que foi copiado, com a execução dos comandos a seguir:

```
php artisan key:generate  
php artisan cache:clear
php artisan migrate
```

Como fizemos novos ajustes na imagem depois do último commit, é o momento para novamente salvar todas as alterações com um novo commit. **Matenha esse PowerShell aberto**. Abra outro PowerShell ou o CMD do Windows e execute o seguinte comando:

```
docker ps
```

Você verá na saída deste comando que o container tem um novo ID. Copie este novo ID e prossiga com o comando a seguir no novo shell que acabou de abrir.

```
docker commit -m "Laravel Crud - Version-2.0" -a "seu nome" NOVO-ID-DO-SEU-CONTAINER seunome/com320-app-v2.0
```

Este processo pode demorar um pouco até finalizar. Após finalizado, execute o comando:

```
docker images
```

E terá uma saída parecida com esta:

```
REPOSITORY                        TAG       IMAGE ID       CREATED        SIZE
julio/com320-app-v2.0             latest    930b815d6fb1   3 hours ago    2.15GB
julio/com320-app-v1.0             latest    930b815d6fb1   3 hours ago    2.05GB
ubuntu                            latest    1318b700e415   2 months ago   72.8MB
hello-world                       latest    d1165f221234   8 months ago   13.3kB
```
Pronto, feito isso, agora você pode fechar o PowerShell anterior que utilizamos para finalizar os úlitmos ajustes.

Vamos na sequência executar a nova imagem com o comando a seguir:

```
docker run -it -p 8080:80 julio/com320-app-v2.0
```

Siginificados:

```
-it - mantém o prompt de comando ativo para o usuário
-p - 8080:80 - especifica que o host vai abrir a porta 8080 para que haja comunicação interna com o container na porta 80. O apache está executando dentro do container na porta 80
- julio/com320-app-v2.0 - é o nome da imagem
```
A saída gerada será um prompt parecido como o que já conhecemos:

```
root@09b523c9ed7c:/# 
```

Vamos então neste prompt iniciar o apache e o mysql:

```
service apache2 start
service mysql start
```

**Matenha o PowerShell aberto** para que possamos testar o front-end que criamos na Semana 3 com o back-end que está na imagem docker. Vamos executar esses passos na próxima seção.

#### Teste do Front-End com o Back-End

Ao final do [Guia da Semana 3](https://gitlab.com/univesp-com320/guias-praticos-semana3/-/tree/main/guia-windows-angular-crud-app#rodando-a-aplica%C3%A7%C3%A3o) aprendemos como executar o front-end. Na ocasião você deveria se perguntar porque não conseguiamos cadastrar os dados via back-end. Isso ocorre porque dividimos a aplicação em duas partes e finalizamos o back-end na Semana 5. Mas faltava toda a ligação que efetuamos neste Guia da Semana 6. Vamos aos testes!!!

##### Passo 1

Antes de inicializar o front-end precisamos fazer uma pequena atualização em um arquivo que indica o caminho do back-end. Acesse a seguinte pasta (Windows):

```
No Windows
C:/laragon/www/angular-crud-app/src/environments
```

Abra o arquivo **environment.ts** e altere a linha:

```
  url_api: 'http://localhost/api/'
```
para

```
  url_api: 'http://localhost:8080/api/'
```

##### Passo 2

**Abra um novo PowerShell**, acesse a pasta da aplicação **angular-crud-app** e execute o comando:

```
npm start
```

A saída gerada deve ser parecida como mostrada a seguir

```
> angular-crud-app@0.0.0 start /var/www/html/angular-crud-app
> ng serve

✔ Browser application bundle generation complete.

Initial Chunk Files                                                  | Names         |      Size
vendor.js                                                            | vendor        |   2.49 MB
polyfills.js                                                         | polyfills     | 128.51 kB
main.js                                                              | main          |  21.86 kB
runtime.js                                                           | runtime       |  12.56 kB
styles.css                                                           | styles        |   1.90 kB

                                                                     | Initial Total |   2.65 MB

Lazy Chunk Files                                                     | Names         |      Size
default-node_modules_angular_forms___ivy_ngcc___fesm2015_forms_js.js | -             | 314.56 kB
src_app_components_universidades_universidades_module_ts.js          | -             |  27.59 kB
src_app_components_pais_pais_module_ts.js                            | -             |  18.35 kB

Build at: 2021-11-11T22:50:04.679Z - Hash: 8cd130606fd9265726be - Time: 13081ms

** Angular Live Development Server is listening on localhost:4200, open your browser on http://localhost:4200/ **


✔ Compiled successfully.
```

##### Passo 3

Veja que na penúltima linha ele indica que para acessarmos o front-end temos que abrir o browser no link: http://localhost:4200/. Faça isso e interaja com a aplicação, realizando o cadastro de países, universidades, realizando remoções e atualizações.

### Conclusão

Espero que todos consigam chegar ao final deste guia e vejam o ambiente em funcionamento. Vejam que não é uma tarefa trivial construir aplicações Web. Mesmo que tenhamos a disposição inúmeros frameworks que facilitam o processo pois encapsulam diversas camadas, ainda assim no começo tudo pode parecer muito obscuro. Isso ocorre pois embora seja mais rápido constuir a aplicação, é preciso conhecer de linguagens como Java Script, entender um pouco do funcionamento dos frameworks de modo a explorá-los e aprender a médio prazo. Que este material seja o primeiro a despertar o seu interesse em aprender ainda mais.
